#include <stdio.h>
int main() {
    char letter;
    int lower, Upper;
    printf("Enter a letter in the alphabet: ");
    scanf("%c", &letter);

    Upper = (letter == 'A' || letter == 'E' || letter  == 'I' ||letter  == 'O' || letter  == 'U');

    lower= (letter == 'a' || letter == 'e' || letter == 'i' || letter  == 'o' ||letter == 'u');

    if (Upper || lower){
        printf("%c is a vowel character.", letter);
    }
    else{
        printf("%c is a consonant character.", letter);
    }
    return 0;
}