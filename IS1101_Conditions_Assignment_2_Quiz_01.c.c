#include <stdio.h>

int main()
{
    int num;
    printf("\nEnter a number:");
    scanf("%d",&num);
    if (num > 0 ){
        printf("\nThis is a positive number = %d",num);
    }
    else if(num < 0){
        printf("\nThis is a negative number = %d",num);
    }
    else{
        printf("\nThis is number zero");
    }
    return 0;
    
}