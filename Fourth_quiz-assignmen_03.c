#include <stdio.h>


int main()
{
    int number,n;
    printf("\nEnter a number:");
    scanf("%d",&number);
    printf("\nFactors of %d are : ",number);
    for(n=1;n<=number;++n){
        if(number%n==0){
            printf(" %d , ", n);
        }
    }
    return 0;
}
